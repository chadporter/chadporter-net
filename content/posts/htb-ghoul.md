+++
date = 2019-10-05T01:00:00-05:00
images = ""
title = "HTB - Ghoul"
tags = ["hackthebox","ghoul","htb","writeups","webapp","web","Gogs","Git","SSH","SSH Pivoting","Bruteforce"]
+++

This post is a write-up for the Ghoul box on hackthebox.eu
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/lsOTa0ozxbEXaKEW_dc6zre.png" alt="" caption="Ghoul Info" class="cld-responsive">

### Enumeration
Start enumerating the ports on the victim machine by running `Nmap` and `Masscan`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/nAhd4zC05KO6m8zJ_rnemza.png" alt="" caption="Running nmap reveals four open ports" class="cld-responsive">

Running nmap reveals the following information:
- Port 22
   - SSH Server
- Port 2222
   - SSH Server
- Port 80
   - Apache web server
- Port 8080
   - Apache Tomcat web server

Poking around the website hosted at port 80 did not result in anything interesting:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/JlPhM8PqdYIIVKai_f0vnhs.png" alt="" caption="Main website" class="cld-responsive">

Browsing to `secret.php` contains a fake user flag:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/hzVUrKzC1yXFbTH9_dvpsz9.png" alt="" caption="`secret.php` website" class="cld-responsive">

The website at port 8080 contains a login! I always try stupid passwords first, and fortunately `admin/admin` worked!
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/ws333i7s6i0yDjMx_iygdxi.png" alt="" caption="Port 8080 Login" class="cld-responsive">

A `Sierra` website is found which immediately presents zip file upload capabilities. After testing a few zip uploads, I realized I had no idea where the files ended up.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/vk3MKAufgTX8ADBJ_gmm8kx.png" alt="" caption="Sierra website" class="cld-responsive">

The [ZipSlip](https://snyk.io/research/zip-slip-vulnerability) vulnerability allows you to upload files to a location that is under your control. Follow the below steps to pop a shell:
1. Place a [PHP Reverse Shell](https://github.com/pentestmonkey/php-reverse-shell) into `/var/www/html` on the attacking machine.
2. Create the zip by running `zip abc123.zip ../../../../../../../var/www/html/rev.php`.
3. Upload `abc123.zip` and call your PHP Reverse Shell:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/7l4oObLE2gmGeBUf_axgikg.png" alt="" caption="Creating and calling PHP Reverse Shell" class="cld-responsive">

### Getting User
Run [LinEnum](https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh) and document the results:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/HYhiHz5ihJV8SxCg_jwgoao.png" alt="" caption="`LinEnum` Output" class="cld-responsive">

Run [LinPEAS](https://github.com/carlospolop/linux-privilege-escalation-awsome-script/tree/master) and document a potential password `test@aogiri123`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/EPfq0dWZVKNVGmtA_sgugpy.png" alt="" caption="`LinPEAS` Output" class="cld-responsive">

`/var/tmp` contains your uploaded .zip file, a pretty strange python script, and a PDF file. `/var/backups/backups` on the other hand contains some interesting things:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/gOSUxOFbCqhcLeym_hzjnie.png" alt="" caption="`/var/backups/backups` directory" class="cld-responsive">

The keys directory contains the SSH Private Keys for `eto`, `kaneki` and `noro`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/tiScrhw0Udr9hDtd_x7c93o.png" alt="" caption="Backup keyfiles" class="cld-responsive">

Try to SSH into the victim machine using the `kaneki.backup` SSH Private Key from the victim machine:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/5dpU5W3Ctt5SwoJJ_mmqzi5.png" alt="" caption="SSH as user `kaneki`" class="cld-responsive">

A SSH Passphrase!? Since the entire box has a theme, use `cewl` to create a wordlist of the `secret.php` page. Then run the following commands to convert `kaneki`'s SSH Private Key to a John readable format, and launch John to bruteforce `kaneki`'s SSH Private Key:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/Pch6OWbA4Kq0sNTd_mvmgw2.png" alt="" caption="Bruteforcing the SSH key using John" class="cld-responsive">

Use the password found to SSH into the victim machine and grab the user flag:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/aIXy9StgU76LP4r2_lups0t.png" alt="" caption="SSH as user `kaneki` and user flag" class="cld-responsive">

### Getting Root
After logging in, notice that `/home/kaneki/note.txt` references a vulnerability in `Gogs`. Since we are not sure where the `Gogs` server is, continue to enumerate the victim machine:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/uo9L9OGCCH0sZQGs_anezga.png" alt="" caption="`Gogs` reference" class="cld-responsive">

During futher enumeration, notice that `/var/tmp/rev.php` was created as root. This should be an "Ah-Ha!" moment, so create another payload using the [ZipSlip](https://snyk.io/research/zip-slip-vulnerability) vulnerability, this time overwriting `/root/.ssh/authorized_keys`, allowing you to SSH into the victim machine as root:
```
ssh-keygen -b 2048 -t rsa -f /root/abc123.key -q -N ""
ssh-keygen -y -f abc123.key > abc123.pub
cat abc123.pub > /root/.ssh/authorized_keys
sudo zip abc1234.zip ../../../../../../../../root/.ssh/authorized_keys
ssh -i abc123.key root@10.10.10.101
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/VUxKwKKuh0fGf6DR_phztm3.png" alt="" caption="Creating `authorized_keys` payload" class="cld-responsive">

Unfortunately, it looks like the root flag does not live on this victim machine (very common in real-world scenarios). Try to find some network users that could give us a clue to another target machine. `/home/kaneki/.ssh/authorized_keys` has a SSH Public Key for the user `kaneki_pub`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/V6G9YTI74wN3DWID_i50cqt.png" alt="" caption="Kaneki's `authorized_keys` file" class="cld-responsive">

Since `Nmap` is not installed, use the below command to scan for devices that are alive in the `172.20.0.0/24` subnet:
```
for ip in $(seq 1 255); do ping -c 1 172.20.0.$ip > /dev/null && echo "Online: 172.20.0.$ip";done
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/n7o9E3P58cJXmPAX_vmjoe0.png" alt="" caption="`172.20.0.0/24` ICMP scan" class="cld-responsive">

SSH into `172.20.0.150` with `kaneki_pub`'s SSH Public Key found in `/home/kaneki/.ssh/authorized_keys` (Assume the same SSH Private Key and Passphrase was used):
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/KUTCGNFHGsIbWWQZ_nlkldu.png" alt="" caption="SSH as user `kaneki_pub`" class="cld-responsive">

As always, start enumerating as this user. `/home/kaneki_pub/to-do.txt` references `AogiriTest` user that might have access to git. Remember the `Gogs` server? Document this user for when you get access to `Gogs`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/88ueMRVMx05TjiWd_xthezg.png" alt="" caption="Contents of `to-do.txt`" class="cld-responsive">

After enumerating this machine and not finding a `Gogs` Server, run `ifconfig` to see if this will give us another machine to attack:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/HPO6jftNHWx7aJU9_jcihng.png" alt="" caption="`kaneki-pc` ifconfig" class="cld-responsive">

`172.18.0.0/24` is a target subnet, run the ICMP command again with this new subnet. The only logical choice is `172.18.0.2`, so that is the target machine. Run the following command to check for open ports on this victim machine:
```
for port in $(seq 1 65535); do (echo > /dev/tcp/172.18.0.2/$port) &> /dev/null && echo "Port $port is open"; done
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/IVDu8ks2x1ikfEwt_rwfiig.png" alt="" caption="`172.18.0.2` port scan" class="cld-responsive">

In the [`Gogs` FAQs](https://gogs.io/docs/intro/faqs), notice that port 3000 is the default port used. Use two SSH forwarders to tunnel from your attacking machine `localhost:3002` -> `Aogiri:3001` -> `kaneki-pc:3000`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/AdkwF54B3XUm9Q1J_lqd43d.png" alt="" caption="SSH forwarders setup" class="cld-responsive">

Kick off a quick `Nmap` scan to make sure the tunnels are working, and that we have the correct target for the `Gogs` server:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/ngOo8pxbPS4XSGuh_kps9di.png" alt="" caption="`Nmap` scan of 172.18.0.2:3000" class="cld-responsive">

Browse to `localhost:3002` and finally find the `Gogs` login! Unfortunately, any interesting `Gogs` exploit has to be run as an authenticated user. You know that `AogiriTest` should have access, so go back to your LinePE results, and grab the potential password `test@aogiri123`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/pRhPDZdhtDRKqY4W_lkmdux.png" alt="" caption="LinePE Result" class="cld-responsive">

Use these credentials to login to the `Gogs` server:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/esOecwonF6fPJkL3_bont8z.png" alt="" caption="Authenticating into `Gogs`" class="cld-responsive">

A quick Google search for `Gogs` vulnerabilities results in [gogsownz](https://github.com/TheZ3ro/gogsownz), which exploits a `Gogs` administrator user's Git Hook and provides RCE! Clone the repository and run the exploit against `http://172.0.0.1:3002/`(Remember the SSH forwarders are still running):
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/OMANPT7krLr1B6bb_lvffmi.png" alt="" caption="`gogsownz` exploit in action" class="cld-responsive">

Check for SUID Bits and Google `/usr/bin/gosu`. You should come across a pretty nifty sudo tool written in Go. Run `gosu -h` and notice that it even makes it easy to execute a privesc directly from the application:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/4VZPVQosuNTaVET1_n00imz.png" alt="" caption="Running `gosu` SUID privesc" class="cld-responsive">

This frustrating victim machine still does not let you just cat the root flag. At least your enumerating skills are getting sharpened. Document the password in `/root/session.sh` and transfer `aogiri-app.7z` to your attacking machine for further analysis:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/GpNUGa9huBiJsKMU_ztrini.png" alt="" caption="Copy `aogiri-app.7z`" class="cld-responsive">

Extract `aogiri-app.7z` and notice that it contains a .git repo in `/aogiri-chatapp`. Firing off some [GitHub Dorks](https://github.com/techgaun/github-dorks) to see if there is any sensitive information leakage did not result in much. A quick Google search resulted in a pretty awesome one-liner using `gitk` and `reflog`:
```
git reflog | awk '{ print $1 }' | xargs gitk
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/lD4YUFhGG8MacQXQ_dn1ood.png" alt="" caption="Scanning for Git Repo sensitive data" class="cld-responsive">

2 Passwords should be found:
- 7^Grc%C\7xEQ?tb4
- g_xEN$ZuWD7hJf2G

Recap all that you have found and exploited:
- Aogiri@172.20.0.10 - rooted
- Kaneki-PC@172.20.0.150/172.18.0.200 - No root
- 3713ea5e4353@172.18.0.2 - root

Use `7^Grc%C\7xEQ?tb4` on `kaneki-pc` to escalate to root:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/zaKJwGfzsdBXeiAp_p14x48.png" alt="" caption="`kaneki-pc` root escalation" class="cld-responsive">

Seriously, a troll at this stage... Use `pspy64` to see if there are any unknown cronjobs running that were not previously shown as the `kaneki_pub` user:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/ctx8iRkcJ0gOmp0v_qpizcv.png" alt="" caption="`pspy64` output" class="cld-responsive">

Interesting. So every 6 minutes an SSH Agent is created. Looking at the service status, the cron service is not active which means the cron is triggered externally, but it allows the system to `ssh root@172.18.0.1 at port 2222`. This seems like a really good place for a [SSH Session Hijack](http://blog.7elements.co.uk/2012/04/ssh-agent-abusing-trust-part-1.html?m=1).

Run the below commands to remove stale agents and hijack the new SSH session, then grab the root flag:
```
rm -rf /tmp/ssh-*
while true; do export SSH_AUTH_SOCK=/tmp$(find . -name 'agent.*'| cut -d "." -f 2-10);ssh root@172.18.0.1 -p 2222;date;sleep 1;done
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1578253066/chadporter.net/HTB/Ghoul/5pMQ9tvHUTV6pS1H_iugn0z.png" alt="" caption="SSH Session Hijack and root flag" class="cld-responsive">