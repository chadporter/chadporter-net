+++
date = 2019-09-28T01:00:00-05:00
title = "HTB - Swagshop"
images = ""
tags = ["hackthebox","swagshop","htb","writeups","Magento","webapp","web","exploit","sudo"]
+++
This post is a write-up for the Swagshop box on hackthebox.eu
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/WknaI3ZfuWABY7q8_tilnlc.png" alt="" caption="Swagshop Info" class="cld-responsive">

### Enumeration

Start enumerating the ports on the victim machine by running `Nmap` and `Masscan`:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/yvJd8JoI4lQ1JDIi_ksdkmk.png" alt="" caption="Running nmap reveals two open ports" class="cld-responsive">

Running nmap reveals the following information:
- Port 22
   - SSH Server
- Port 80
   - Seems to be running a web server.

Open a web browser to view the webapp:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/OPfsLM2ylB0UY3xz_ug8e1f.png" alt="" caption="Magento Shopping Cart" class="cld-responsive">

I had a quick look around the application and couldn’t come up with anything useful to gain access to the admin page (`http://10.10.10.140/index.php/admin`) which was found from a gobuster scan. A quick Google search results in a [Magento Shoplift SQLi](https://github.com/joren485/Magento-Shoplift-SQLI) exploit. This looks like Magento could possibly contain a SQL injection to create a new user and password to gain access to the admin section.

Git clone the exploit and run it:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/KSF5rGfezVCJPjfA_xdox3s.png" alt="" caption="Running modified Magento eCommerce - Remote Code Execution exploit" class="cld-responsive">

Login to the webapp with the credentials provided to access to the admin portal.
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/qYr6twzsN04e71xN_rm5hrv.png" alt="" caption="Magento Admin" class="cld-responsive">

I had a good look around to see if I could find anything but reverted to looking for a public exploit for this. I eventually came across an article that mentions taking advantage of an upload function to get a reverse shell. https://blog.scrt.ch/2019/01/24/magento-rce-local-file-read-with-low-privilege-admin-rights/

### Gaining a shell
Go to `Catalog` > `Manage Products`. Continue to add a new product with an arbitrary name. Make sure you fill in all of the relevant information to make sure you can add your product to the shopping cart:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/PKDxABCHBHMDZqR4_cgsrgi.png" alt="" caption="New Product Settings" class="cld-responsive">

<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/acUx4HGxDyfXetwH_oyencs.png" alt="" caption="New Product Settings" class="cld-responsive">

<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/jxpXrhdlDAAaMKGI_llfveq.png" alt="" caption="New Product Settings" class="cld-responsive">

<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/4qCZ7tWqKMviSbwg_tn1f2x.png" alt="" caption="New Product Settings" class="cld-responsive">

After filling in all of the requirements, browse to the product on the front page. Grab a [PHP Reverse Shell](https://github.com/pentestmonkey/php-reverse-shell), modify the parameters, rename the file to `rev.phtml`, upload it on the shopping cart page, and click on add to cart:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/s7Q4k3dQlrzwpY5m_t31t06.png" alt="" caption="Magento Shopping Cart" class="cld-responsive">

Great! Now where did our reverse shell end up? Looking at the article, the `.phtml` file is uploaded to `http://10.10.10.140/media/custom_options/quote/quote/firstLetterOfYourOriginalFileName/secondLetterOfYourOriginalFileName`.

Fire up a nc listener, find the upload, and click on it:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/qYNUeyGSrzjLAMBv_tsm5hw.png" alt="" caption="Upload link location" class="cld-responsive">

Grab the user flag:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/hKuKBHCDxSDU6dUj_rpzrmw.png" alt="" caption="User flag" class="cld-responsive">

### Root flag

With a user shell on the victim machine, go through the [Rebootuser Local Linux Enumeration & Privilege Escalation Cheatsheet](https://www.rebootuser.com/?p=1623), and notice an interesting response to `sudo -l`. The user has sudo permissions to run `vi` only on file in `/var/www/html/*`. See what files you can edit to possibly escalate privileges:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/DF2gw5R5eT7SLsDU_addc3c.png" alt="" caption="Directory listing of `/var/www/html/`" class="cld-responsive">

You can use any file in `/var/www/html/`, as you are simply using it for a shell escape. Run `sudo /usr/bin/vi /var/www/html/cron.sh`, and add `set shell=/bin/sh` into the script:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/GWJTZ5Kaqt4tcqhO_bt34mf.png" alt="" caption="Directory listing of `/var/www/html/`" class="cld-responsive">

Exit insert mode, run `:shell` to escape into a root shell, and grab the root flag:
<img data-src="https://res.cloudinary.com/dvi2dcepy/image/upload/w_auto,c_scale,f_auto,q_auto/v1577459926/chadporter.net/HTB/SwagShop/0vW3r6FmpkKdOhYk_vebwnu.png" alt="" caption="Root flag" class="cld-responsive">