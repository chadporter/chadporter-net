---
name: Chad Porter
images: ["rxSPDze7TGS4CwTn.jpg"]
twitter: 'spcchad'
aliases: 
    - /about/
---

[**GPG Fingerprint**: 60F8 07ED 1C35 3351 CD7D 219E 3BFF 2ACB 26EB B0E4](https://keybase.io/chadaporter)

Current Information Security Analyst II at [**Pratum**](https://pratum.com).

8+ years experience specializing in network engineering, infrastructure automation, cybersecurity, penetration testing, and scientific hooliganism.

Super nerd who does not just “press the ‘pwn’ button”, exploits at scale while remaining stealthy, and loves identifying and exploiting misconfigurations in network infrastructure.

Interested in devising better problem-solving methods for challenging tasks, and protecting valuable data through comprehensive and real world scenario testing.