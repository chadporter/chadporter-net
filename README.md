<div align="center">
  <h1>Chad Porter Website</h1>
</div>

<p align="center">
  My personal website built with Hugo and :heart:
</p>

<div align="center">
  <a href="https://app.netlify.com/sites/jovial-jepsen-8c93c3/deploys">
    <img alt="Netlify Status" src="https://api.netlify.com/api/v1/badges/ce4734ed-49e4-4af6-97b5-9f54dc3427b2/deploy-status" />
  </a>
  <a href="https://creativecommons.org/licenses/by-sa/4.0/">
    <img alt="CC BY-SA 4.0" src="https://img.shields.io/badge/license-CC%20BY--SA%204.0-lightgrey.svg" />
  </a>
</div>

<br />


## License

Provided under the terms of the CC BY-SA 4.0 License.

Copyright © 2019, [Chad Porter](https://chadporter.net).